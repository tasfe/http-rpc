package com.haha.http.rpc.common.util;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * @author zhangxiaofan07
 * @version 1.0 2022/2/10
 */
public class JsonUtils {
    public static String toJson(Object o) {
        return new Gson().toJson(o);
    }

    public static <T> T toObject(String target, Class<T> clazz) {
        return new Gson().fromJson(target, clazz);
    }

    public static <T> T toObject(String target, Type type) {
        return new Gson().fromJson(target, type);
    }
}
