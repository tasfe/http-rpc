package com.haha.http.rpc.common;

import java.io.Serializable;

/**
 * @AUTHOR zhangxiaofan07
 * @CREATE 2022-01-27 20:26
 */
public class Response implements Serializable {

    private static final long serialVersionUID = -4772670584904284645L;
    private static final String SUCCESS_CODE = "SUCCESS";
    private static final String SUCCESS_MSG = "成功";
    private final boolean success;
    private final String code;
    private final String msg;
    private String data;

    private Response() {
        this(true, SUCCESS_CODE, SUCCESS_MSG);
    }

    private Response(boolean success, String code, String msg) {
        this.success = success;
        this.code = code;
        this.msg = msg;
    }

    private Response(String data) {
        this(true, SUCCESS_CODE, SUCCESS_MSG, data);
    }

    private Response(boolean success, String code, String msg, String data) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static Response error(String code, String msg) {
        return new Response(false, code, msg);
    }

    public static Response success() {
        return new Response();
    }

    public static Response success(String data) {
        return new Response(data);
    }

    public boolean isSuccess() {
        return success;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Response{" +
                "success=" + success +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
